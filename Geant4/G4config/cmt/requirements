package        G4config
version        v96r4p4
branches       cmt doc


#=============================================================================
# set dependencies
#===========================================================================
use GaudiPolicy v*                  -no_auto_imports
use CLHEP       v* LCG_Interfaces

#nkw 
#-- Databases
use Geant4Files v96r*               -no_auto_imports

#
#==============================================================================
# set versions of Geant4
#==============================================================================
set G4_native_version     "9.6.p04" \
    override-geant4-version     "${G4_NATIVE_VERSION}"
set G4VERS v96r4p4

# =============================================================================
# set Geant4 environment variables
#=============================================================================
set  G4PATH      "/afs/cern.ch/sw/lcg/external/geant4"

set  G4SHARE     "${G4PATH}/${G4_native_version}/share"

set  G4SRC       "${G4SHARE}/source" 

# Copy command for various UNIX flavours
set G4_UNIX_COPY " cp -a " Darwin " cp -PpR "

# =============================================================================
# ============ general macros =================================================
# =============================================================================

macro   G4config_pp_cppflags    " -DG4_STORE_TRAJECTORY " \
	target-winxp ' /D"G4_STORE_TRAJECTORY" /D"XPNET" /D"G4LIB_BUILD_DLL" '

tag target-dbg G4verbose

macro_append G4config_pp_cppflags  "" \
	target-winxp&G4verbose ' /D"G4VERBOSE" ' \
	G4verbose " -DG4VERBOSE " 

macro X11R6       "/usr/X11R6"
macro X11         "/usr/include/X11"
macro X11include  "-I$(X11R6)/include -I$(X11)/extensions -I$(X11)"

macro_prepend GEANT4_install_include " $(GEANT4_home)/$(GEANT4_installarea_prefix)/include " 

# ============================================================================
# ============ "combined patterns" ============================================
# =============================================================================
macro G4LibraryFlags           ""  

pattern        G4AllCMTpatterns \
               apply_pattern  package_stamps    ;\ 
               apply_pattern  packageDir        ;\
               apply_pattern  library_Softlinks library=<package> ;\
               apply_pattern  linker_library    library=<package> ;\
               apply_pattern  package_linkopts ;\ 
               apply_pattern  package_shlibflags 

#=============================================================================
# =================  the end ==================================================
# =============================================================================

pattern G4_copy_source \
     action <package>_source_copy "$(G4CONFIGROOT)/cmt/copy_source.csh" ; \
     macro_append <package>_dependencies " <package>_source_copy "


pattern G4_copy_pattern \
       action <package>_<type>_source_copy "$(G4CONFIGROOT)/cmt/copy_<type>_source.csh" ; \
       macro_append <package>_dependencies " <package>_<type>_source_copy "

pattern G4_copy_UI_source \
     apply_pattern G4_copy_pattern type=UI

pattern G4_copy_vis_source \
     apply_pattern G4_copy_pattern type=vis

pattern G4_copy_proc_source \
     apply_pattern G4_copy_pattern type=proc

pattern G4_copy_hadlists_source \
     apply_pattern G4_copy_pattern type=hadlists

pattern G4_copy_gdml_source \
     apply_pattern G4_copy_pattern type=gdml

pattern G4_copy_examples_source \
     apply_pattern G4_copy_pattern type=examples

private

action G4config_copy_include "$(G4CONFIGROOT)/cmt/copy_include.csh"
macro_append constituents " G4config_copy_include "

end_private

#!/usr/bin/env csh

#set echo on

set G4PSRC = "${G4SRC}/processes"
set here = $PWD

set pack = `cmt -quiet show macro_value package | tr -d "G4"`
if ($pack == "progen") then
   cd ${G4PSRC}
   set dir = $PWD
   set list1 = `find . -maxdepth 2 -name src -print | sed -e 's;^./;;'`
   set list = "${list1} electromagnetic/utils/src"
else
  cd ${G4PSRC}/${pack}
  set dir = $PWD
  set list = `find . -name src -print | sed -e 's;^./;;'`
endif
cd $here/..
set n = 0
foreach p ($list[*])
set test = `echo $p | awk '{print(index($0,"test/"))}'`
  if ($test == "0") then
   if !( -d $p ) then
    mkdir -p $p
    $G4_UNIX_COPY ${dir}/${p}/*.* ${p}/.
    $G4_UNIX_COPY ${dir}/${p}/../include/*.* G4${pack}/.
   else
#  echo ' ' ${dir)/${p} ' exists - skip copy'
    @ n++
  endif
endif
end
if ( $n <= 0 ) then 
  echo ' source files have been copied from '${dir}
else 
  echo ' source files exist - NO copy'
endif

cd $here

unset echo


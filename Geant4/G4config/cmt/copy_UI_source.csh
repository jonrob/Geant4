#!/usr/bin/env csh

#set echo on
#copy files from G4Dir = interfaces
set G4Dir = "interfaces"

set here = $PWD
 
set pack = `cmt -quiet show macro_value package`
if ( "$pack" == "G4UIbasic" || "$pack" == "G4UIcommon" ) then
   set pp = `echo $pack | tr -d "G4UI"`
   set p = "${pp}/src"
else if ("$pack" == "G4UIGAG" ) then
   set p = "GAG/src"
endif  


cd $here/..

if !( -d $p) then
  mkdir -p $p
  $G4_UNIX_COPY ${G4SRC}/${G4Dir}/${p}/*.* ${p}/.
  $G4_UNIX_COPY ${G4SRC}/${G4Dir}/${p}/../include/*.* ${pack}/.
  echo ' source files have been copied from '${G4SRC}'/'${G4Dir}
else
  echo ' ' ${G4SRC}/${G4Dir}/${p} ' exists - skip copy'
endif

cd $here

unset echo


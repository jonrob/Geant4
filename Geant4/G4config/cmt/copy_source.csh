#!/usr/bin/env csh

#set echo on

set here = $PWD
set package = `cmt -quiet show macro_value package`
set pack = `cmt -quiet show macro_value package | tr -d "G4"`
if ( "$pack" == "parmodels" ) set pack = "parameterisations"

cd ${G4SRC}/${pack}
set list = `find . -name src -print | sed -e 's;^./;;'`
set incl = `find . -name include -print | sed -e 's;^./;;'`
cd $here/..

set n = 0
foreach p ($list[*])
set test = `echo $p | awk '{print(index($0,"test/"))}'`
  if ($test == "0") then
    if !( -d $p ) then
      mkdir -p $p 
      $G4_UNIX_COPY ${G4SRC}/${pack}/${p}/*.* ${p}/.
    else
#      echo ' ' ${G4SRC}/${pack}/${p} ' exists - skip copy'
      @ n++
    endif
  endif
end
if ( $n <= 0 ) then 
  echo ' source files have been copied from '${G4SRC}'/'${pack}
else 
  echo ' source files exist - NO copy'
endif
#
# copy include files if not yet done
if !( -f ${package}.stamp) then 
  foreach p ($incl[*])
   set test = `echo $p | awk '{print(index($0,"test/"))}'`
   set abla = `echo $p | awk '{print(index($0,"/abla/"))}'`
#   echo ${p} ', test = ' ${test} ', abla = ' ${abla}
   if ($test == "0" ) then
      if ($abla == "0" ) then
         $G4_UNIX_COPY ${G4SRC}/${pack}/${p}/*.* ${package}/.
      endif
   endif
  end
  touch ${package}.stamp
  echo ' include files have been copied from '${G4SRC}'/'${pack}
else 
  echo ' include files exist - NO copy'
endif

#

cd $here

# NKW 22/02/2014
#Not pretty but will work.
if ( $pack == "processes" || $pack == "physics_lists" ) then
  echo "Copying patched sources"
   ./copyPatchedSource.py
endif
unset echo



#!/usr/bin/env csh

set echo on

#copy files from G4Dir = visualization
set G4Dir = "visualization"

set here = $PWD
 
set pack = `cmt -quiet show macro_value package | tr -d "G4"`

if ( "$pack" == "VRML" || "$pack" == "modeling" || "$pack" == "RayTracer" || "$pack" == "Tree" ) then
   set p = "${pack}/src"
else if ("$pack" == "OpenL" ) then
   set pack = "OpenGL"
   set p = "OpenGL/src"
else if ("$pack" == "FR") then
   set p = "FukuiRenderer/src"
else if ("$pack" == "vis_management") then
   set p = "management/src"
else if ("$pack" == "visHepRep") then
   set p = "HepRep/src"
else if ("$pack" == "visXXX") then
   set p = "XXX/src"
else if ("$pack" == "externals" ) then
   set p = "externals"
endif  


cd $here/..
if !( -d $p) then
  mkdir -p $p
  if ( $p != 'externals' ) then 
    $G4_UNIX_COPY ${G4SRC}/${G4Dir}/${p}/*.* ${p}/.
    $G4_UNIX_COPY ${G4SRC}/${G4Dir}/${p}/../include/*.* G4${pack}/.
#nkw Pick up any headers that are missing from the default share/include
    $G4_UNIX_COPY ${G4SRC}/${G4Dir}/${p}/../include/*.* ../../InstallArea/${CMTCONFIG}/include/
    echo ' source files have been copied from '${G4SRC}'/'${G4Dir}
    echo ' and headers also copied to InstallArea'
  else
    cd ${G4SRC}/${G4Dir}/$p
    set list = `find . -name src -print | sed -e 's;^./;;' | sed -e 's/\/src//g'`
    echo 'List  ' $list
    cd $here/..
    set n = 0
    foreach s ($list[*])
      if (! -d $s ) then
        echo $s
        mkdir -p $p/$s/src
        $G4_UNIX_COPY ${G4SRC}/${G4Dir}/${p}/${s}/src/*.* ${p}/${s}/src/.
        $G4_UNIX_COPY ${G4SRC}/${G4Dir}/${p}/${s}/include/*.* G4${pack}/.
      else
        @ n++
      endif
    end
  endif
else
  echo ' ' ${G4SRC}/${G4Dir}/${p} ' exists - skip copy'
endif

cd $here

unset echo


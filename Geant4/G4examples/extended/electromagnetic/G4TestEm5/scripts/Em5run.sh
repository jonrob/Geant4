#!/bin/bash
if [[ $# -eq 0 ]] ; then
    echo 'Running Multiple Scattering Test'
    echo 'Number of runs for uncertainties not specified, using defauly value of 1000'
    n=1000
fi

if [[ $# -eq 1 ]] ; then
    n=$1
    echo 'Running Multiple Scattering test'
fi

CurrentDir="${PWD##*/}"
echo $CurrentDir
TestDir=MSCTest
if [[ ! -d "MSCTest" && $CurrentDir != $TestDir ]]; then
mkdir $TestDir
cd $TestDir
fi

if [[ -d "MSCTest" && $CurrentDir != $TestDir ]]; then
cd $TestDir
fi

MacGen.py $n
testEm5.exe BruteForce.mac $n





Active Integral for Histogram 1=  5.40286
Absorber Integral for Histogram 1=  37.7518
sampling fraction for energy 44.44GeV = 0.143115
--------------------------------
Active Integral for Histogram 2=  3.04413
Absorber Integral for Histogram 2=  21.2765
sampling fraction for energy 25GeV = 0.143075
--------------------------------
Active Integral for Histogram 3=  1.94739
Absorber Integral for Histogram 3=  13.6318
sampling fraction for energy 16GeV = 0.142856
--------------------------------
Active Integral for Histogram 4=  1.35372
Absorber Integral for Histogram 4=  9.47209
sampling fraction for energy 11.11GeV = 0.142917
--------------------------------
Active Integral for Histogram 5=  0.996114
Absorber Integral for Histogram 5=  6.95917
sampling fraction for energy 8.16GeV = 0.143137
--------------------------------
Active Integral for Histogram 6=  0.762283
Absorber Integral for Histogram 6=  5.3328
sampling fraction for energy 6.25GeV = 0.142942
--------------------------------
Active Integral for Histogram 7=  0.601039
Absorber Integral for Histogram 7=  4.2165
sampling fraction for energy 4.94GeV = 0.142545
--------------------------------
Active Integral for Histogram 8=  0.488023
Absorber Integral for Histogram 8=  3.41442
sampling fraction for energy 4GeV = 0.14293
--------------------------------
Active Integral for Histogram 9=  0.40268
Absorber Integral for Histogram 9=  2.8257
sampling fraction for energy 3.31GeV = 0.142506
--------------------------------
Active Integral for Histogram 10=  0.33765
Absorber Integral for Histogram 10=  2.37472
sampling fraction for energy 2.78GeV = 0.142185
--------------------------------
Active Integral for Histogram 11=  0.2874
Absorber Integral for Histogram 11=  2.02471
sampling fraction for energy 2.37GeV = 0.141946
--------------------------------
Active Integral for Histogram 12=  0.248803
Absorber Integral for Histogram 12=  1.74186
sampling fraction for energy 2.04GeV = 0.142838
--------------------------------
Active Integral for Histogram 13=  0.217333
Absorber Integral for Histogram 13=  1.51913
sampling fraction for energy 1.78GeV = 0.143064
--------------------------------

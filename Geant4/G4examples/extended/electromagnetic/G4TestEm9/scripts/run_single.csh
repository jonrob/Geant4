#/bin/csh
#================================================
#     Macro file for hadr00 run over all Physics Lists
#     26.06.2009 V.Ivanchneko
#================================================
#Run with: source run_single.csh TestEm9

rm -f $1.out
$G4CONFIGROOT/../G4examples/extended/electromagnetic/G4TestEm9/$CMTCONFIG/testEm9.exe $1.in >& $1_test.out 

#
